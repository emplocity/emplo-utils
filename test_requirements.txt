-r requirements.txt
black==23.12.0
coverage==7.3.2
fasttext==0.9.2
isort==5.12.0
mypy==1.7.1
pre-commit==3.6.0
pytest==7.4.3
pytest-cov==4.1.0
ruff==0.1.8
types-python-dateutil==2.8.19.14
types-requests==2.31.0.10
