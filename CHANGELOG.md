# Release history

## 8.0.0
 - Support for SSO tokens passed by cookies in RequestFactory and GraphqlClient.


## 7.8.0
 - Bump filedepot and dev dependencies

## 7.7.0
 - Add `FakeDispatcher` class to replace `nameko.events.EventDispatcher`

## 7.6.2
 - Use pyproject.toml for project setup

## 7.6.0
 - Update test requirements and typing fixes

## 7.5.2
 - Added new extensions to file upload: .webp, .webm, .svg

## 7.4.2
 - Nameko cors cleanup and ruff linter

## 7.0.0
 - Add initial support for nameko 3.0 RC

## 6.1.0
 - Small linting and testing fixes

## 6.0.0
 - Move `FileStorage` and `FileUploader` to file.py

## 5.1.0
 - Add `FileStorage` class for `filedepot` package to manage file upload/download

## 5.0.0
Breaking changes:
 - Change the type of input args and return values in `FileUploader`,
   `blur_photo` and `make_avatar` to `pathlib.Path`
 - Add `TextLinesProvider`, `JsonProvider`, `FastTextModelProvider` to
   data science dependencies
 - `MultiModelProvider` takes another provider as the first argument and uses
   it to wrap multiple paths; this is a simpler alternative to mixin-based
   approach used previously

## 4.1.0
 - add support for Python 3.9, Werkzeug 2.x and pandas 1.x

## 4.0.0
 - `emplo.testing.GraphqlClient` supports file uploads **only** when using
   `Upload` scalar, as in https://github.com/lmcgartland/graphene-file-upload

## 3.0.0

 - add missing type annotations almost everywhere
 - remove APIs that weren't cleaned up in 2.0:
   - emplo.dependencies.graphql_endpoint
   - functions from emplo.utils related to file uploads; use emplo.image APIs
   - emplo.utils.get_caller_name

## 2.2.0

 - Add compatibility with Werkzeug 1.0

## 2.1.1

 - fix version number in `__init__`

## 2.1.0

 - Use proper Bearer header for token-based auth in tests

## 2.0.0

 - remove GraphQL and graphene-related APIs; these are available as
   separate emplo-graphene package

## 1.2.2

 - fixed packaging issue where 1.2.1 was released as 1.2.0

## 1.2.1

 - adjust version ranges in some of the requirements to comply with new pip
   dependency resolver

## 1.2.0
 - fix RelayPagination has_next_page property. Some tests added

## 1.1.0

 - allow passing arbitrary context to GraphQLEndpoint.query

## 1.0

 - remove APIs deprecated in 0.31.0:
   - emplo.service.Service
   - emplo.service.is_service_responsive
   - emplo.managers
   - emplo.dependencies.db
   - emplo.utils.transaction_scope
   - emplo.utils.session_scope
   - emplo.utils.get_or_create

## 0.33.0

 - don't display original error message in Graphql error response

## 0.32.0
 - refactor emplo.graphene.filtering module
 - add some tests to emplo.graphene.filtering
 - filter by conjunction of arguments, when
   symbol & in argument

## 0.31.0

 - deprecate all APIs that will be removed in 1.0:
   - emplo.service.Service
   - emplo.service.is_service_responsive
   - emplo.managers
   - emplo.dependencies.db
   - emplo.utils.transaction_scope
   - emplo.utils.session_scope
   - emplo.utils.get_or_create

## 0.30.1

 - fix typing issue in private Manager method

## 0.30.0

 - log parsing and execution exceptions in GraphqlEndpoint

## 0.29.1
 - performance improvments in RequestFactory

## 0.29.0
 - remove emplo.managers.FileUploader; use emplo.image.FileUploader which
   doesn't depend on the database

## 0.28.2
  - bump version in emplo/__init__.py

## 0.28.0

 - new version of FileUploader not depending on Manager implementation.
   upload() method returns path to uploaded file

## 0.27.0

 - remove async_call_exception_handler and promise_result_exception_handler
   decorators

## 0.26.0

 - add RequestFactory to builc mock requests compatible with werkzeug

## 0.25.4

 - fix FileUploader raise message when FileIsTooLarge. Some tests added

## 0.25.3

 - MockFileStorage replaced by create_autospec(FileStorage)

## 0.25.2

 - FileUploader.upload method for better encapsulation. FileSizeValidator
   tests made more explicit

## 0.25.0

 - FileSizeValidator class to check uploading (url or bytes) file size

## 0.24.1

 - bump version in emplo/__init__.py


## 0.24.0

 - added CompanySlugify class to generate simple company short_name


## 0.23.0

 - added is_service_responsive() method as a simple circuit breaker
   for nameko service proxies

## 0.22.0

 - added emplo.custom_types module with some common type definitions,
   such as DatetimeIsoformat, InstanceId, TimeRange

## 0.21.2

 - upgraded emplo-nameko-zipkin to 0.1.7 which should fix monkey patching
   of urllib.request

## 0.21.1

 - fixed AttributeError in GraphQLEndpoint

## 0.21.0

 - added PEP 561 compliant type marker

## 0.20.0

 - renamed CorsHttpRequestHandler.response_from_result() to
   response_from_result_and_request() to avoid breaking API contract

## 0.19.0

 - removed deprecated image functions from emplo.utils; use their variants
   from emplo.image instead

## 0.18.1

 - fix incorrect reporting of short transactions as errors

## 0.18.0

 - added session_scope() context manager
 - session_scope() and transaction_scope() log their duration

## 0.17.0

- added managers.FileUploader to save file from url or bytes(request)

## 0.15.0

- added error_type when request raise any exception

## 0.14.0

 - added MemoryLogger to log datascience models memory usage

## 0.13.0

 - added DataframeProvider to datascience utils

## 0.12.0

 - added remove_duplicates_from_list and remove_duplicates_from_dict_list
   to help with dictionary cleanup

## 0.11.2

 - raise exception from saved_file_from_url when request fails

## 0.11.1

 - added get_or_create() as a convenience Manager method

## 0.11.0

 - reformat source code with Black

## 0.10.1

 - add session.flush() in managers delete method

## 0.8.1

 - import graphene on demand when calling Manager.get_by_gid

## 0.8.0

 - switched to emplo-nameko-zipkin for Zipkin integration

## 0.7.0

 - private method in Manager for converting orderBy args to underscore


## 0.6.1

 - move SQLAlchemy import to avoid issues where sqlalchemy is not installed

## 0.6.0

 - added MultiModelProvider to allow for example multi-language models

## 0.5.1

 - fixed method name in ModelProvider

## 0.5.0

 - added datascience utils - ModelProvider and DependencyReplaceProvider

## before 0.5.0

this is prehistory :)
