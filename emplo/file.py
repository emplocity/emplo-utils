from pathlib import Path
from typing import Optional, Union
from urllib.parse import urljoin

import requests
from depot.io.interfaces import StoredFile  # type: ignore
from depot.manager import DepotManager  # type: ignore
from werkzeug.datastructures import FileStorage

from emplo.image import saved_file_from_request, saved_file_from_url

Url = str


class FileTypeError(Exception):
    pass


class FileIsTooLarge(Exception):
    pass


class DepotFileStorage:
    def __init__(self, app_url: str):
        self._depot_manager = DepotManager.get()
        self._app_url = app_url

    def store(
        self, input_file: Union[bytes, Path], filename: Optional[Path] = None
    ) -> StoredFile:
        if isinstance(input_file, Path):
            with open(input_file, mode="rb") as f:
                input_file = f.read()
        file_id = self._depot_manager.create(input_file, filename)
        return self._depot_manager.get(file_id)

    def retrieve_url(self, file: StoredFile) -> Optional[str]:
        if not file:
            return None
        if file.public_url:
            return file.public_url
        else:
            return urljoin(self._app_url, f"depot/{file.file_id}")

    def retrieve_file(self, file_id: str) -> StoredFile:
        return self._depot_manager.get(file_id)


class FileSizeValidator:
    def __init__(self, max_size_kb: int):
        self.max_size_kb = max_size_kb

    def validate(self, input_file: Union[Url, FileStorage]) -> bool:
        file_size = 0
        if isinstance(input_file, Url):
            file_size = self._get_file_url_size(input_file)
        elif isinstance(input_file, FileStorage):
            file_size = self._get_uploaded_file_size(input_file)

        if file_size > self.max_size_kb or file_size < 0:
            return False
        return True

    def _get_file_url_size(self, file_url: Url) -> int:
        try:
            r = requests.get(file_url, stream=True, timeout=3)
            r.raise_for_status()
            return int(r.headers["Content-Length"]) // 1000
        except requests.HTTPError:
            return -1

    def _get_uploaded_file_size(self, uploaded_file: FileStorage) -> int:
        if uploaded_file.content_length:
            return uploaded_file.content_length // 1000
        try:
            original_position = uploaded_file.tell()
            uploaded_file.seek(0, 2)
            size = uploaded_file.tell()
            uploaded_file.seek(original_position)
            return size // 1000
        except (AttributeError, IOError):
            raise


class FileUploader:
    def __init__(self, max_size_kb: Optional[int] = None):
        self.file_size_validator: Optional[FileSizeValidator]
        if max_size_kb:
            self.file_size_validator = FileSizeValidator(max_size_kb)
        else:
            self.file_size_validator = None

    def upload(self, input_file: Union[Url, FileStorage]) -> Path:
        if self.file_size_validator and not self.file_size_validator.validate(
            input_file
        ):
            raise FileIsTooLarge(
                f"Input file exceeding max size limit: {self.file_size_validator.max_size_kb}KB"
            )
        if isinstance(input_file, Url):
            return self._upload_from_url(input_file)
        elif isinstance(input_file, FileStorage):
            return self._upload_from_file(input_file)
        else:
            raise FileTypeError(
                f"Input file type must be URL (str) or FileStorage, not: {type(input_file)}"
            )

    def _upload_from_file(self, uploaded_file: FileStorage) -> Path:
        with saved_file_from_request(uploaded_file) as file_path:
            return file_path

    def _upload_from_url(self, file_url: Url) -> Path:
        with saved_file_from_url(file_url) as file_path:
            return file_path
