import collections
from typing import Any, Dict, List, Mapping, MutableMapping


def update_dict(
    d: MutableMapping[Any, Any], u: Mapping[Any, Any]
) -> MutableMapping[Any, Any]:
    """
    Updates deeply nested dict in place.

    d - nested dict
    u - key-value updating element
    example
    profile = {"basics": {"name": "marian ma", "location": {"city": "Warszawa"}}}
    name = {"basics": {"name": "Jarek test"}}
    update_dict(profile, name)
    """

    for k, v in u.items():
        if isinstance(v, Mapping):
            r = update_dict(d.get(k, {}), v)
            d[k] = r
        else:
            d[k] = u[k]
    return d


def remove_deep_keys_from_dict(d: Dict[Any, Any], keys: List[Any]) -> None:
    """
    Removes deeply nested keys from dict in place.
    """
    if len(keys) > 1:
        remove_deep_keys_from_dict(d[keys[0]], keys[1:])
    elif len(keys) == 1:
        if isinstance(d, list):
            for el in d:
                remove_deep_keys_from_dict(el, keys)
            d.remove({})
        elif isinstance(d, dict):
            if keys[0] in d:
                del d[keys[0]]


def remove_none_values_from_dict(d: Dict[Any, Any]) -> Dict[Any, Any]:
    """
    Removes key:value pairs where value is None.

    Works on deeply nested dicts or lists of dicts.
    """
    if not isinstance(d, (dict, list)):
        return d
    if isinstance(d, list):
        return [v for v in (remove_none_values_from_dict(v) for v in d) if v]
    return {
        k: v
        for k, v in ((k, remove_none_values_from_dict(v)) for k, v in d.items())
        if v is not None
    }


def remove_duplicates_from_dict_list(array: List[Dict]) -> List[Dict]:
    return list({frozenset(item.items()): item for item in array}.values())


def remove_duplicates_from_list(array: List[str]) -> List[str]:
    """
    Remove duplicates from list with preserving order
    """
    return list(collections.OrderedDict.fromkeys(array))
