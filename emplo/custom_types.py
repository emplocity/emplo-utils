from datetime import date, datetime
from typing import Dict, Union

DatetimeIsoformat = str
DateIsoformat = str
InstanceId = int
TimeRange = Dict[str, Union[datetime, date, DatetimeIsoformat, DateIsoformat]]
URLType = str
