from datetime import datetime

from dateutil.relativedelta import relativedelta

WEEKDAYS = 7


class TimespanRange:
    def __init__(self, start: datetime, end: datetime):
        self.start = start
        self.end = end


class Period:
    name = "Unknown"

    def __init__(self):
        self.now = datetime.now()
        self._previous_range: TimespanRange
        self._current_range: TimespanRange
        self._calculate_previous_range()
        self._calculate_current_range()

    @property
    def previous_range(self) -> TimespanRange:
        return self._previous_range

    @property
    def current_range(self) -> TimespanRange:
        return self._current_range

    def _calculate_previous_range(self) -> None:
        raise NotImplementedError

    def _calculate_current_range(self) -> None:
        raise NotImplementedError


class TodayPeriod(Period):
    name = "today"

    def _calculate_previous_range(self) -> None:
        _start = datetime(
            self.now.year, self.now.month, self.now.day, 0, 0, 0, 0
        ) + relativedelta(days=-1)
        _end = self.now + relativedelta(days=-1)
        self._previous_range = TimespanRange(start=_start, end=_end)

    def _calculate_current_range(self) -> None:
        _start = datetime(self.now.year, self.now.month, self.now.day, 0, 0, 0, 0)
        _end = self.now
        self._current_range = TimespanRange(start=_start, end=_end)


class YesterdayPeriod(Period):
    name = "yesterday"

    def _calculate_previous_range(self) -> None:
        _start = datetime(
            self.now.year, self.now.month, self.now.day, 0, 0, 0, 0
        ) + relativedelta(days=-2)
        _end = _start + relativedelta(days=1)
        self._previous_range = TimespanRange(start=_start, end=_end)

    def _calculate_current_range(self) -> None:
        _start = datetime(
            self.now.year, self.now.month, self.now.day, 0, 0, 0, 0
        ) + relativedelta(days=-1)
        _end = _start + relativedelta(days=1)
        self._current_range = TimespanRange(start=_start, end=_end)


class CurrentWeekPeriod(Period):
    name = "current_week"

    def _calculate_previous_range(self) -> None:
        today = datetime(self.now.year, self.now.month, self.now.day, 0, 0, 0, 0)
        _start = today + relativedelta(days=-self.now.weekday() - WEEKDAYS)
        _end = self.now + relativedelta(days=-WEEKDAYS)
        self._previous_range = TimespanRange(start=_start, end=_end)

    def _calculate_current_range(self) -> None:
        today = datetime(self.now.year, self.now.month, self.now.day, 0, 0, 0, 0)
        _start = today + relativedelta(days=-self.now.weekday())
        _end = self.now
        self._current_range = TimespanRange(start=_start, end=_end)


class PreviousWeekPeriod(Period):
    name = "previous_week"

    def _calculate_previous_range(self) -> None:
        today = datetime(self.now.year, self.now.month, self.now.day, 0, 0, 0, 0)
        _start = today + relativedelta(days=-self.now.weekday() - 2 * WEEKDAYS)
        _end = _start + relativedelta(days=WEEKDAYS)
        self._previous_range = TimespanRange(start=_start, end=_end)

    def _calculate_current_range(self) -> None:
        today = datetime(self.now.year, self.now.month, self.now.day, 0, 0, 0, 0)
        _start = today + relativedelta(days=-self.now.weekday() - WEEKDAYS)
        _end = _start + relativedelta(days=WEEKDAYS)
        self._current_range = TimespanRange(start=_start, end=_end)


class CurrentMonthPeriod(Period):
    name = "current_month"

    def _calculate_previous_range(self) -> None:
        _start = datetime(self.now.year, self.now.month, 1, 0, 0, 0, 0) + relativedelta(
            months=-1
        )
        _end = self.now + relativedelta(months=-1)
        self._previous_range = TimespanRange(start=_start, end=_end)

    def _calculate_current_range(self) -> None:
        _start = datetime(self.now.year, self.now.month, 1, 0, 0, 0, 0)
        _end = self.now
        self._current_range = TimespanRange(start=_start, end=_end)


class PreviousMonthPeriod(Period):
    name = "previous_month"

    def _calculate_previous_range(self) -> None:
        _start = datetime(self.now.year, self.now.month, 1, 0, 0, 0, 0) + relativedelta(
            months=-2
        )
        _end = _start + relativedelta(months=1)
        self._previous_range = TimespanRange(start=_start, end=_end)

    def _calculate_current_range(self) -> None:
        _start = datetime(self.now.year, self.now.month, 1, 0, 0, 0, 0) + relativedelta(
            months=-1
        )
        _end = _start + relativedelta(months=1)
        self._current_range = TimespanRange(start=_start, end=_end)


class EntirePeriod(Period):
    name = "entire"

    def _get_range(self) -> TimespanRange:
        _start = datetime(2018, 1, 1, 0, 0, 0, 0)
        _end = self.now
        return TimespanRange(start=_start, end=_end)

    def _calculate_previous_range(self) -> None:
        self._previous_range = self._get_range()

    def _calculate_current_range(self) -> None:
        self._current_range = self._get_range()


class CorrespondingPeriods:
    def __init__(self, period_name: str):
        self._period_cls = self._identify_period_class(period_name)
        self._now = self._period_cls.now

    @property
    def previous_range(self) -> TimespanRange:
        return self._period_cls.previous_range

    @property
    def current_range(self) -> TimespanRange:
        return self._period_cls.current_range

    def _identify_period_class(self, period_name: str) -> Period:
        for period_cls in Period.__subclasses__():
            if period_cls.name == period_name:
                return period_cls()
        return Period()
