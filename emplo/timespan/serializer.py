import time
from datetime import datetime
from typing import Any, Dict

from .period import TimespanRange


def from_datetime_to_timestamp(dt: datetime) -> int:
    return int(time.mktime(dt.timetuple()))


class TimespanRangeSerializer:
    def __init__(self, timespan_range: TimespanRange):
        self._timespan_range = timespan_range

    def __call__(self) -> Dict[str, Any]:
        return self.serialize()

    def serialize(self) -> Dict[str, Any]:
        raise NotImplementedError


class DatetimeTimespanRangeSerializer(TimespanRangeSerializer):
    def serialize(self) -> Dict[str, datetime]:
        return {"start": self._timespan_range.start, "end": self._timespan_range.end}


class TimestampTimespanRangeSerializer(TimespanRangeSerializer):
    def serialize(self) -> Dict[str, int]:
        return {
            "start": from_datetime_to_timestamp(self._timespan_range.start),
            "end": from_datetime_to_timestamp(self._timespan_range.end),
        }


class StringTimespanRangeSerializer(TimespanRangeSerializer):
    def serialize(self) -> Dict[str, str]:
        return {
            "start": self._timespan_range.start.isoformat(),
            "end": self._timespan_range.end.isoformat(),
        }
