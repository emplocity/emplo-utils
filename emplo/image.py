import logging
import mimetypes
import shutil
import tempfile
from contextlib import contextmanager
from pathlib import Path
from typing import IO, Any, Iterator, Tuple

import requests
from PIL import Image, ImageFile, ImageFilter
from werkzeug.datastructures import FileStorage

from .custom_types import URLType

ImageFile.LOAD_TRUNCATED_IMAGES = True

logger = logging.getLogger(__name__)

mimetypes.init()


def blur_photo(filepath: Path, blur_value: int = 4) -> Path:
    image = Image.open(filepath)
    blurred_image = image.filter(ImageFilter.GaussianBlur(blur_value))
    blurred_image.save(filepath)
    return filepath


def make_avatar(filepath: Path, size: Tuple[int, int] = (128, 128)) -> Path:
    image = Image.open(filepath)
    image = image.convert("RGB")
    image.thumbnail(size, Image.Resampling.BILINEAR)
    if image.mode in ("RGBA", "LA"):
        background = Image.new(image.mode[:-1], image.size, "#FFF")
        background.paste(image, image.split()[-1])
        image = background
    image.save(filepath)
    return filepath


@contextmanager
def saved_file_from_request(opened_file: FileStorage) -> Iterator[Path]:
    extension = guess_extension(opened_file)
    try:
        with _save_to_temp_file(opened_file.stream, extension) as filename:
            yield filename
    except Exception as e:
        logger.exception(e)
        raise IOError(f"Cannot execute request on the file: {opened_file.filename}")


@contextmanager
def saved_file_from_url(url: URLType) -> Iterator[Path]:
    try:
        response = requests.get(url, timeout=3, stream=True)
        response.raise_for_status()
        content_type = response.headers["content-type"]
        extension = mimetypes.guess_extension(content_type)
        if not extension:
            raise IOError(f"Cannot find out extension of the url: {url}")
        response.raw.decode_content = True
        with _save_to_temp_file(response.raw, extension) as filename:
            yield filename
    except Exception as e:
        logger.exception(e)
        raise IOError(f"Cannot download file from URL: {url}") from e


@contextmanager
def _save_to_temp_file(file_content: IO[Any], extension: str) -> Iterator[Path]:
    with tempfile.NamedTemporaryFile(suffix=extension, delete=False) as temp_file:
        shutil.copyfileobj(file_content, temp_file)
        temp_file.flush()
        yield Path(temp_file.name)


def guess_extension(opened_file: FileStorage) -> str:
    extension = mimetypes.guess_extension(opened_file.mimetype)
    if extension:
        return extension
    if opened_file.filename:
        extension = opened_file.filename.split(".")[-1]
        if extension not in [
            "jpeg",
            "jpg",
            "png",
            "doc",
            "docx",
            "pdf",
            "txt",
            "rtf",
            "webp",
            "webm",
        ]:
            raise IOError(
                f"Cannot find out extension of the file: {opened_file.filename}"
            )
        return f".{extension}"
    raise IOError(f"Failed to detect extension of {opened_file}")
