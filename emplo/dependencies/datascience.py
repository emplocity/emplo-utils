import json
import logging
import pickle
from pathlib import Path
from typing import Any, Dict, List

import pandas as pd
from nameko.containers import WorkerContext
from nameko.exceptions import ExtensionNotFound
from nameko.extensions import DependencyProvider

try:
    import fasttext
except ImportError:
    fasttext = None

from emplo.memory import MemoryLogger

logger = logging.getLogger(__name__)


Model = Any


class ModelProvider(DependencyProvider):
    """
    Loads a machine learning model on service startup.

    Allows to start the service even when model doesn't exist or
    isn't trained yet. The service needs to check that the provided
    model is not None before using it.

    The base class provides facilities to log memory usage or detect if
    model file actually exists.
    Child classes should implement `load()` method which does actual
    model loading - takes filesystem path and returns model instance.

    For example using gensim's Doc2Vec model:
        >>> class Doc2VecProvider(ModelProvider):
        ...     def load(self, model_path: Path) -> Doc2Vec:
        ...         return Doc2Vec.load(str(model_path))

    In terms of nameko service lifecycle, model is loaded in setup(),
    once per service. Subsequent usage via get_dependency() uses an already
    loaded model.
    """

    def __init__(self, path: Path, **kwargs):
        super().__init__(**kwargs)
        self.model: Model = None
        self.path = path

    def setup(self) -> None:
        if not self.path.exists():
            logger.warning(f"Model does not exist: path={self.path}")
            return
        with MemoryLogger().log_memory_increase(
            f"{self.__class__.__name__} Model loaded: path={self.path}"
        ):
            self.model = self.load(self.path)

    def get_dependency(self, worker_ctx: WorkerContext) -> Model:
        return self.model

    def load(self, path: Path) -> Model:
        raise NotImplementedError


class TextLinesProvider(ModelProvider):
    """
    Reads a text file and provides a list of lines.
    """

    def load(self, path: Path) -> Model:
        with path.open("r") as f:
            return f.readlines()


class JsonProvider(ModelProvider):
    """
    Loads a JSON-formatted file as a Python object (dict/list/etc.)
    """

    def load(self, path: Path) -> Model:
        with path.open("r") as f:
            return json.load(f)


class PickleModelProvider(ModelProvider):
    """
    Loads a pickled Python object from a file.

    WARNING: Use this dependency only with files you absolutely trust!
    Pickle can execute arbitrary code on deserialization and should be
    considered unsafe.
    """

    def load(self, path: Path) -> Model:
        with path.open("rb") as f:
            return pickle.load(f)


class FastTextModelProvider(ModelProvider):
    def load(self, path: Path) -> Model:
        if not fasttext:
            logger.warning(f"Cannot load model, fasttext is not installed: {path=}")
            return None
        return fasttext.load_model(str(path))


class MultiModelProvider(DependencyProvider):
    """
    Loads multiple machine learning models on service startup.

    In contrast to `ModelProvider`, this provider allows loading
    many related models at once - for example multi-language versions
    of same model. The service needs to check that the provided models
    are not None before using them.

    >>> class MyService(Service):
    ... models = MyProvider(PickleModelProvider, {
    ...     'foo': BASE_PATH / 'foo.p',
    ...     'bar': BASE_PATH / 'bar.p',
    ... })
    """

    def __init__(self, provider_class, paths: Dict[str, Path], **kwargs):
        self.providers = {key: provider_class(path) for key, path in paths.items()}

    def setup(self) -> None:
        for provider in self.providers.values():
            provider.setup()
        logger.info(f"Loaded multiple models: count={len(self.providers)}")

    def get_dependency(self, worker_ctx: WorkerContext) -> Dict[str, Model]:
        return {
            key: provider.get_dependency(worker_ctx)
            for key, provider in self.providers.items()
        }


class DependencyReplaceProvider(DependencyProvider):
    """
    Utility provider which allows reloading/replacing other providers.

    One possible use case is to reload a machine learning model
    after training. As model files tend to be massive, it isn't
    feasible to load them on demand. Therefore the whole provider
    should be replaced so that setup() is called again.

    Example:

    >>> class DeckardService(Service):
    ...     model = VoightKampffProvider()
    ...     replacer = DependencyReplaceProvider()
    ...
    ...     @event_handler('deckard_service', 'training_finished')
    ...     def reload_model(self, new_model_filename):
    ...         new_model_provider = VoightKampffProvider(new_model_filename)
    ...         self.replacer.replace('model', new_model_provider)
    """

    def replace(self, old_provider_name: str, new_provider: DependencyProvider) -> None:
        container = self.container
        try:
            old_provider = [
                dep
                for dep in container.dependencies
                if dep.attr_name == old_provider_name
            ].pop()
        except IndexError:
            raise ExtensionNotFound(
                f'No dependency with name "{old_provider_name}" bound to the service.'
            )
        self._bind_provider(new_provider, old_provider.attr_name)
        container.dependencies.remove(old_provider)
        container.dependencies.add(new_provider)

    def _bind_provider(self, provider: DependencyProvider, name: str) -> None:
        """
        Reassign new provider to current container under `name`.
        """
        provider.container = self.container
        provider.attr_name = name
        provider.setup()

    def get_dependency(self, worker_ctx: WorkerContext) -> "DependencyReplaceProvider":
        return self


class DataframeProvider(DependencyProvider):
    def __init__(self, path: Path, columns: List[str], sep: str = "|"):
        self.path = path
        self.columns = columns
        self.sep = sep
        self.df = None

    def setup(self) -> None:
        if not self.path.exists():
            logger.warning(f"Data file does not exist: path={self.path}")
            return
        with MemoryLogger().log_memory_increase(f"Data file loaded: path={self.path}"):
            self.df = pd.read_csv(
                self.path, sep=self.sep, header=None, names=self.columns
            )

    def get_dependency(self, worker_ctx: WorkerContext) -> pd.DataFrame:
        return self.df
