from jinja2 import Environment, FileSystemLoader
from nameko.containers import WorkerContext
from nameko.extensions import DependencyProvider


class TemplatesEnvProvider(DependencyProvider):
    """
    Provides jinja2 templates environment that caches templates.
    Usage inside a service:

    template = self.templates_env.get_template("index.html")
    template.render()

    """

    def __init__(self, templates_dir_path: str, **kwargs):
        self.templates_dir_path = templates_dir_path
        super(TemplatesEnvProvider, self).__init__(**kwargs)

    def setup(self) -> None:
        template_loader = FileSystemLoader(searchpath=self.templates_dir_path)
        self.template_env = Environment(loader=template_loader, auto_reload=False)

    def get_dependency(self, worker_ctx: WorkerContext) -> Environment:
        return self.template_env
