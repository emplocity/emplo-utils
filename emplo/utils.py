import re
from urllib.parse import urlencode

from .custom_types import URLType

first_cap_re = re.compile("(.)([A-Z][a-z]+)")
all_cap_re = re.compile("([a-z0-9])([A-Z])")


def get_change(current: float, previous: float) -> float:
    if current == previous:
        return 0.0
    try:
        return 100.0 * (current - previous) / previous
    except ZeroDivisionError:
        return 100.0


def camel_case_to_underscore(text: str) -> str:
    s1 = first_cap_re.sub(r"\1_\2", text)
    return all_cap_re.sub(r"\1_\2", s1).lower()


def make_url(base_url: URLType, *res: str, **params: str) -> URLType:
    url = base_url
    for r in res:
        url = "{}/{}".format(url, r)
    if params:
        url = "{}?{}".format(url, urlencode(params))
    return url
