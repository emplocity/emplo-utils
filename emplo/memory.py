import contextlib
import logging
import resource
from typing import Iterator

logger = logging.getLogger(__name__)


class MemoryLogger:
    """
    Helper class to kep an eye on memory usage.

    Linux-only, due to :py:func:`resource.getrusage`.
    """

    def log_memory(self, message: str) -> None:
        """
        Logs total current memory in use by the service.

        :param message: message to be logged along with memory usage
        """
        memory = self._get_memory_usage() / 1024
        logger.info(f"{message}, memory_usage_mb={memory:,.2f}")

    @contextlib.contextmanager
    def log_memory_increase(self, message: str) -> Iterator[None]:
        """
        Logs how much memory usage increased when running the block.

        Example:

        >>> with MemoryLogger().log_memory_increase("Heavy computation"):
        ...     heavy_computation()

        :param message: message to be logged along with memory increase
        """
        rss_before = self._get_memory_usage()
        yield
        rss_after = self._get_memory_usage()
        memory_used = (rss_after - rss_before) / 1024
        logger.info(f"{message}, memory_increase_mb={memory_used:,.2f}")

    def _get_memory_usage(self) -> int:
        return resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
