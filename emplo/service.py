import types
from functools import partial
from typing import Any, Iterable

from eventlet.event import Event
from nameko.exceptions import BadRequest
from nameko.extensions import register_entrypoint
from nameko.web.handlers import HttpRequestHandler
from werkzeug.wrappers import Request, Response


class CorsHttpRequestHandler(HttpRequestHandler):
    """
    A Cors Http handler.
    Registers an OPTIONS route per endpoint definition.
    """

    def __init__(
        self,
        method: str,
        url: str,
        expected_exceptions: Iterable[Exception] = (),
        **kwargs,
    ):
        super(CorsHttpRequestHandler, self).__init__(
            method, url, expected_exceptions=expected_exceptions
        )
        self.debug: bool = kwargs.get("debug", True)
        self.cors_enabled: bool = kwargs.get("cors_enabled", False)
        if self.cors_enabled:
            self.allowed_origin: list[str] = kwargs.get("origin", ["*"])
            self.allowed_methods: list[str] = kwargs.get("methods", ["*"])
            self.allow_credentials: bool = kwargs.get("credentials", True)

    def handle_request(self, request: Request) -> Response:
        if request.method == "OPTIONS":
            return self.response_from_result_and_request(result="", request=request)

        request.shallow = False
        try:
            context_data = self.server.context_data_from_headers(request)
            args, kwargs = self.get_entrypoint_parameters(request)

            self.check_signature(args, kwargs)
            event = Event()
            self.container.spawn_worker(
                self,
                args,
                kwargs,
                context_data=context_data,
                handle_result=partial(self.handle_result, event),
            )
            result = event.wait()

            response = self.response_from_result_and_request(
                result=result, request=request
            )

        except Exception as exc:
            response = self.response_from_exception(exc)
        return response

    def response_from_result_and_request(
        self, result: Any, request: Request
    ) -> Response:
        """
        We can't override response_from_result() while keeping stateless
        request argument. This is why this custom method exists.
        """

        response = super(CorsHttpRequestHandler, self).response_from_result(result)

        if self.cors_enabled:
            response.headers.add(
                "Access-Control-Allow-Headers",
                request.headers.get("Access-Control-Request-Headers", ""),
            )
            response.headers.add(
                "Access-Control-Allow-Credentials", str(self.allow_credentials).lower()
            )
            response.headers.add(
                "Access-Control-Allow-Methods", ",".join(self.allowed_methods)
            )
            response.headers.add(
                "Access-Control-Allow-Origin", ",".join(self.allowed_origin)
            )
        return response

    def response_from_exception(self, exc):
        if self.debug:
            return super(CorsHttpRequestHandler, self).response_from_exception(exc)
        if isinstance(exc, self.expected_exceptions) or isinstance(exc, BadRequest):
            status_code = 400
        else:
            status_code = 500
        payload = f"Error {status_code}"

        return Response(
            payload,
            status=status_code,
        )

    @classmethod
    def decorator(cls, *args, **kwargs):
        """
        We're overriding the decorator classmethod to allow it to register an options
        route for each standard REST call. This saves us from manually defining OPTIONS
        routes for each CORs enabled endpoint
        """

        def registering_decorator(fn, args, kwargs):
            instance = cls(*args, **kwargs)
            register_entrypoint(fn, instance)
            if kwargs.get("cors_enabled", False):
                if instance.method in ("GET", "POST", "DELETE", "PUT") and (
                    "*" in instance.allowed_methods
                    or instance.method in instance.allowed_methods
                ):
                    options_args = ["OPTIONS"] + list(args[1:])
                    options_instance = cls(*options_args, **kwargs)
                    register_entrypoint(fn, options_instance)
            return fn

        if len(args) == 1 and isinstance(args[0], types.FunctionType):
            # usage without arguments to the decorator:
            # @foobar
            # def spam():
            #     pass
            return registering_decorator(args[0], args=(), kwargs={})
        else:
            # usage with arguments to the decorator:
            # @foobar('shrub', ...)
            # def spam():
            #     pass
            return partial(registering_decorator, args=args, kwargs=kwargs)


cors_http = CorsHttpRequestHandler.decorator
