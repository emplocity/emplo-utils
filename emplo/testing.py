import os
from enum import Enum
from json import dumps, loads
from typing import Any, Dict, Optional
from unittest.mock import MagicMock, Mock, create_autospec

from nameko.containers import ServiceContainer
from werkzeug.datastructures import FileStorage, MultiDict
from werkzeug.wrappers import Request


def replace_dependencies(container: ServiceContainer, **kwargs) -> None:
    """
    Usage:
        container = container_factory(MyService, my_config)
        redis = StrictRedis(host='localhost')
        replace_dependencies(container, redis=redis)
        container.start()
    """
    for dep_name, new_dependency in kwargs.items():
        try:
            dep_to_replace = [
                dep for dep in container.dependencies if dep.attr_name == dep_name
            ][0]
            container.dependencies.remove(dep_to_replace)

            mock_dependency = Mock()
            mock_dependency.attr_name = dep_name
            mock_dependency.get_dependency.return_value = new_dependency
            container.dependencies.add(mock_dependency)
        except (KeyError, IndexError):
            raise ValueError(
                "Container has not got a dependency called {0}".format(dep_name)
            )


class RequestFactory:
    """
    Creates a mock werkzeug-compatible Request.

    Use it as a pytest fixture, for example:

    >>> @pytest.fixture
    >>> def request_factory():
    ...     return RequestFactory()
    """

    def __call__(
        self,
        method: str = "GET",
        form: Optional[Dict[str, Any]] = None,
        json: Optional[Any] = None,
        args: Optional[Dict[str, Any]] = None,
    ) -> Request:
        request = MagicMock(spec=Request)
        request.method = method
        request.args = {} if args is None else args
        request.headers = {}
        request.cookies = {}
        if form is not None:
            request.mimetype = "multipart/form-data"
            request.form = MultiDict(form)
        elif json is not None:
            request.mimetype = "application/json"
            request.data = dumps(json).encode("utf-8")
        request.get_data = MagicMock(return_value=request.data)
        request.user_agent.string = "request_factory"
        request.access_route = ["127.0.0.1"]
        request.referrer = ""
        return request


class TokenSource(Enum):
    HEADER = "header"
    COOKIE = "cookie"


class GraphqlClient:
    def __init__(
        self,
        db_session,
        endpoint,
        repositories=None,
        rpc_proxies=None,
        token_source=TokenSource.HEADER,
        sso_cookie_prefix="emplo",
    ):
        self.db_session = db_session
        self.endpoint = endpoint
        self.repositories = repositories
        self.rpc_proxies = rpc_proxies
        self.token_source = token_source
        self.sso_cookie_prefix = sso_cookie_prefix

    def build_request(self, query, token=None, files=None):
        """
        Builds a mock HTTP request ready to pass to GraphQLView.

        Allows for file uploads using ``Upload`` scalar - see
        https://github.com/lmcgartland/graphene-file-upload for details.

        Keys of ``files`` dictionary must correspond to ``variables``
        defined in the GraphQL query.
        """
        request_factory = RequestFactory()
        multipart_data = {"operations": dumps(query)}
        content_length = 0
        file_storages = {}
        if files:
            file_map = {}
            for key, (filename, fp, mimetype) in files.items():
                file_map[key] = [f"variables.{key}"]
                multipart_data[key] = fp
                content_length += os.stat(
                    os.path.join(os.path.dirname(fp.name), filename)
                ).st_size
                storage = create_autospec(
                    FileStorage, filename=filename, stream=fp, mimetype=mimetype
                )
                file_storages[key] = storage
            multipart_data["map"] = dumps(file_map)
        request = request_factory(method="POST", form=multipart_data)
        if token is not None:
            if self.token_source == TokenSource.COOKIE:
                request.cookies[f"{self.sso_cookie_prefix}_access-token"] = token
            else:
                request.headers["Authorization"] = f"Bearer {token}"
        request.headers["Content-Length"] = content_length
        request.files = file_storages
        return request

    def send_protected_query(self, query, token=None, **kwargs):
        view = self.endpoint
        request = self.build_request(query, token, files=kwargs.get("files", None))
        response = view.query(
            request, self.db_session, self.repositories, self.rpc_proxies, **kwargs
        )
        response = loads(response.data)
        try:
            return response
        except KeyError:
            raise RuntimeError(response["errors"])


class FakeDispatcher:
    """
    A replacement for ``nameko.events.EventDispatcher``.

    Simulates dispatching nameko events, but doesn't interact with RabbitMQ.
    """

    def __init__(self):
        self.events = {}

    def __call__(self, event_name: str, event_payload: dict[Any, Any]) -> None:
        self.events[event_name] = event_payload

    def assert_event_not_dispatched(self, event_name) -> None:
        assert event_name not in self.events

    def assert_event_dispatched(
        self, event_name, event_payload: Optional[dict[Any, Any]] = None
    ) -> None:
        assert event_name in self.events
        if event_payload:
            for key, value in event_payload.items():
                assert self.events[event_name][key] == value
