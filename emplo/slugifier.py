import re
import string

from slugify import slugify


class CompanySlugify:
    def slugify(self, name: str) -> str:
        name = name.lower()
        name = name.replace("-", " ")
        name = self._remove_diacritics(name)
        name = self._remove_multiple_whitespaces(name)
        name = self._remove_company_legal_forms(name)
        name = self._remove_company_legal_forms_shortcuts(name)
        return slugify(text=name, separator="")

    def _remove_diacritics(self, name: str) -> str:
        translation_table = str.maketrans({key: None for key in string.punctuation})
        return name.translate(translation_table)

    def _remove_multiple_whitespaces(self, name: str) -> str:
        return re.sub(r"\s+", " ", name)

    def _remove_company_legal_forms(self, name: str) -> str:
        legal_forms = [
            "spółka z ograniczoną odpowiedzialnością",
            "z ograniczoną odpowiedzialnością",
            "spółka z ograniczona odpowiedzialnością",
            "spółka z ograniczona odpowiedzialnoscia",
            "spółka komandytowa",
            "spółka akcyjna",
            "spółka jawna",
            "spółka z o o",
            "spółka z oo",
            "spółka zoo",
            "spółka k",
            "spółka c",
            "spolka akcyjna",
            "spolka z ograniczona odpowiedzialnoscia",
            "spolka komandytowa",
            "spolka akcyjna",
            "spolka jawna",
            "spolka z o o",
            "spolka z oo",
            "spolka zoo",
            "spolka k",
            "spolka c",
            "®",
            "sp z oo",
            "sp zoo",
            "sp z o o",
            "sp z oo",
            "spzoo",
            "spz o o",
            "spz oo",
            "polska",
            "poland",
            "towarzystwo funduszy inwestycyjnych",
        ]

        name = re.sub(r"|".join(legal_forms), "", name)
        return name

    def _remove_company_legal_forms_shortcuts(self, name: str) -> str:
        legal_forms_shortcuts = [
            " sa ",
            " sa$",
            " ^sa",
            " s a ",
            "s a$",
            "^s a ",
            " sc ",
            " sc$",
            "^sc ",
            " s c ",
            " s c$",
            "^s c ",
            " spk ",
            " spk$",
            "^spk ",
            " sp k ",
            " sp k$",
            "^sp k ",
            " sp j ",
            " sp j$",
            "^sp j ",
            " spj ",
            " spj$",
            "^spj ",
            " pol ",
            " sj ",
            " sj$",
            "^sj ",
        ]
        name = re.sub(r"|".join(legal_forms_shortcuts), " ", name)
        name = re.sub(r"\s+", " ", name).rstrip().lstrip()
        return name
