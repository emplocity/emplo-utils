import pickle
from pathlib import Path
from unittest.mock import Mock

import fasttext
import pandas as pd
import pytest
from nameko.containers import WorkerContext

from emplo.dependencies.datascience import (
    DataframeProvider,
    FastTextModelProvider,
    JsonProvider,
    MultiModelProvider,
    PickleModelProvider,
    TextLinesProvider,
)


class Model:
    def __init__(self, label):
        self.label = label

    def predict(self):
        return self.label


@pytest.fixture
def worker_context() -> WorkerContext:
    return Mock(spec=WorkerContext)


def test_dataframe_provider(tmp_path: Path, worker_context: WorkerContext) -> None:
    path = tmp_path / "dataframe.csv"
    df = pd.DataFrame({"foo": [40, 2], "bar": [True, False]})
    df.to_csv(path, sep="|", header=False)

    provider = DataframeProvider(path, columns=["foo", "bar"], sep="|")
    provider.setup()
    df = provider.get_dependency(worker_context)
    assert df["foo"].sum() == 42


def test_textlines_provider(tmp_path: Path, worker_context: WorkerContext) -> None:
    path = tmp_path / "words.txt"
    path.write_text("spam\nham\neggs")

    provider = TextLinesProvider(path)
    provider.setup()
    lines = provider.get_dependency(worker_context)
    assert lines[2] == "eggs"


def test_json_provider(tmp_path: Path, worker_context: WorkerContext) -> None:
    path = tmp_path / "data.json"
    path.write_text('{"root": {"node": [9, 10, 11]}}')

    provider = JsonProvider(path)
    provider.setup()
    payload = provider.get_dependency(worker_context)
    assert payload["root"]["node"][0] == 9


def test_pickle_model_provider(tmp_path: Path, worker_context: WorkerContext) -> None:
    path = tmp_path / "data.p"
    pickle.dump(Model(label="unsafe"), path.open("wb"))

    provider = PickleModelProvider(path)
    provider.setup()
    model = provider.get_dependency(worker_context)
    assert model.predict() == "unsafe"


def test_fasttext_model_provider(tmp_path: Path, worker_context: WorkerContext) -> None:
    data_path = tmp_path / "data.txt"
    data_path.write_text("__label__dwarf Gimli\n__label__elf Legolas")
    model = fasttext.train_supervised(str(data_path))
    path = tmp_path / "model.bin"
    model.save_model(str(path))
    provider = FastTextModelProvider(path)
    provider.setup()
    model = provider.get_dependency(worker_context)
    assert "__label__dwarf" in model.labels


def test_multi_model_provider(tmp_path: Path, worker_context: WorkerContext) -> None:
    path_pl = tmp_path / "data_pl.p"
    pickle.dump(Model(label="niebezpieczne"), path_pl.open("wb"))
    path_en = tmp_path / "data_en.p"
    pickle.dump(Model(label="unsafe"), path_en.open("wb"))

    provider = MultiModelProvider(PickleModelProvider, {"pl": path_pl, "en": path_en})
    provider.setup()
    models = provider.get_dependency(worker_context)
    assert models["pl"].predict() == "niebezpieczne"
    assert models["en"].predict() == "unsafe"
