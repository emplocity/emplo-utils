import eventlet

eventlet.monkey_patch()

import tempfile

import pytest
from PIL import Image

image_extensions = [".jpeg", ".jpg", ".png", ".webp"]


@pytest.fixture(params=image_extensions)
def temp_image(request):
    with tempfile.NamedTemporaryFile(delete=True, suffix=request.param) as f:
        image = Image.new("RGB", size=(50, 50), color=(123, 123, 123))
        image.save(f)
        f.seek(0)
        yield f
