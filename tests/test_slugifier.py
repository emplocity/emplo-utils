import pytest

from emplo.slugifier import CompanySlugify


@pytest.mark.parametrize(
    "company_name, slugged_company_name",
    [
        ("budimex", "budimex"),
        ("aviva", "aviva"),
        ("santander", "santander"),
        ("cenatorium", "cenatorium"),
        ("enprom", "enprom"),
        ("forte", "forte"),
        ("emplocity", "emplocity"),
        ("mbank", "mbank"),
        ("pko", "pko"),
        ("neuca", "neuca"),
        ("bnpparibas", "bnpparibas"),
        ("dpd", "dpd"),
        ("bgk", "bgk"),
        ("bankbnpparibas", "bankbnpparibas"),
        ("orbis", "orbis"),
        ("tueuropa", "tueuropa"),
        ("aliorbank", "aliorbank"),
        ("natek", "natek"),
        ("contman", "contman"),
        ("cinkciarzpl", "cinkciarzpl"),
        ("fabrykaformy", "fabrykaformy"),
        ("franke", "franke"),
        ("umbrella", "umbrella"),
        ("cbb", "cbb"),
        ("polo polska", "polo"),
        ("test", "test"),
    ],
)
def test_company_slugify_with_current_clients(company_name, slugged_company_name):
    slugifier = CompanySlugify()
    assert slugged_company_name == slugifier.slugify(company_name)


@pytest.mark.parametrize(
    "company_name, slugged_company_name",
    [
        ("bank bnp paribas", "bankbnpparibas"),
        ("KFC", "kfc"),
        ("Bar kość i ość", "barkosciosc"),
        ("Emplocity spółka zoo", "emplocity"),
        ("Emplocity spółka z oo", "emplocity"),
        ("Emplocity spółka z o o", "emplocity"),
        ("Emplocity spółka z o. o.", "emplocity"),
        ("Emplocity spółka z o.o.", "emplocity"),
        ("Emplo-c_i_t_y", "emplocity"),
        ("**  EMPLO CITY!  **", "emplocity"),
        ("tramwaje-warszawskie-sp-z-o-o", "tramwajewarszawskie"),
        ("budokrusz-spolka-akcyjna", "budokrusz"),
        ("opoltrans-spolka-z-o-o", "opoltrans"),
        ("hs-multiservices-spolka-z-ograniczona-odpowiedzialnoscia", "hsmultiservices"),
    ],
)
def test_company_slugify_with_different_names(company_name, slugged_company_name):
    slugifier = CompanySlugify()
    assert slugged_company_name == slugifier.slugify(company_name)
