import imghdr
from pathlib import Path
from unittest.mock import create_autospec

import pytest
from werkzeug.datastructures import FileStorage

from emplo.file import FileUploader
from emplo.image import blur_photo, make_avatar


def test_make_avatar(temp_image):
    image_path = Path(temp_image.name)
    avatar_path = make_avatar(image_path)
    assert temp_image.name == str(avatar_path)


def test_blur_photo(temp_image):
    image_path = Path(temp_image.name)
    avatar_path = blur_photo(image_path)
    assert temp_image.name == str(avatar_path)


@pytest.fixture
def mock_file_storage(temp_image):
    return create_autospec(
        FileStorage,
        filename=temp_image.name,
        stream=temp_image,
        mimetype="image/" + temp_image.name.split(sep=".")[1],
    )


def test_save_file_from_request(mock_file_storage):
    uploader = FileUploader()
    file_path = uploader.upload(mock_file_storage)
    assert file_path.suffix in [".jpg", ".png", ".webp"]
    assert imghdr.what(file_path) in ["jpeg", "png", "webp"]
