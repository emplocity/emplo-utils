import contextlib
import socket

import pytest
from nameko.rpc import rpc
from nameko.web.handlers import http

from emplo.service import cors_http

try:
    from nameko import config

    NAMEKO_V3 = True
except ImportError:
    NAMEKO_V3 = False


def is_port_open(port: int, host: str = "localhost") -> bool:
    with contextlib.closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as sock:
        sock.settimeout(1)
        result = sock.connect_ex((host, port))
        return result == 0


RABBITMQ_PORT = 5672
require_rabbitmq = pytest.mark.skipif(
    not is_port_open(RABBITMQ_PORT),
    reason=f"RabbitMQ is not running on port {RABBITMQ_PORT}",
)


class TestService:
    name = "test_service"

    @http("GET", "/hello")
    def http_endpoint(self, _):
        return "Hello from HTTP"

    @cors_http("GET", "/cors_enabled", cors_enabled=True, origin=["*"])
    def http_cors(self, request):
        return "Hello from cors"

    @cors_http("GET", "/cors_disabled")
    def http_cors_disabled(self, request):
        return "Hello from disabled cors"

    @rpc
    def rpc_endpoint(self):
        return "Hello from RPC"


@pytest.fixture
def container_config(rabbit_config, web_config):
    if NAMEKO_V3:
        return config
    else:
        cfg = rabbit_config.copy()
        cfg.update(web_config)
        return cfg


@pytest.fixture()
def service_container(container_factory, container_config):
    container = container_factory(TestService, container_config)
    container.start()
    return container


@require_rabbitmq
def test_http(service_container, web_session):
    response = web_session.get("/hello")
    assert response.status_code == 200
    assert response.text == "Hello from HTTP"


@require_rabbitmq
def test_http_cors(service_container, web_session):
    response = web_session.get("/cors_enabled")
    assert response.status_code == 200
    assert response.headers["Access-Control-Allow-Origin"] == "*"
    assert response.text == "Hello from cors"


@require_rabbitmq
def test_http_cors_disabled(service_container, web_session):
    response = web_session.get("/cors_disabled")
    assert response.status_code == 200
    assert "Access-Control-Allow-Origin" not in response.headers
    assert response.text == "Hello from disabled cors"
