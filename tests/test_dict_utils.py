from typing import Any

import pytest

from emplo.dict_utils import (
    remove_deep_keys_from_dict,
    remove_duplicates_from_dict_list,
    remove_duplicates_from_list,
    remove_none_values_from_dict,
    update_dict,
)


@pytest.fixture
def data():
    return {"outer": {"inner": "value"}, "other": "can't touch this"}


def test_update_dict(data):
    to_update = {"outer": {"inner": "updated"}}
    expected = {"outer": {"inner": "updated"}, "other": "can't touch this"}
    update_dict(data, to_update)
    assert data == expected


def test_remove_deep_keys_from_dict(data):
    expected = {"outer": {}, "other": "can't touch this"}
    remove_deep_keys_from_dict(data, ["outer", "inner"])
    assert data == expected


def test_remove_deep_keys_from_dict_top_level(data):
    expected = {"other": "can't touch this"}
    remove_deep_keys_from_dict(data, ["outer"])
    assert data == expected


def test_remove_none_values_from_dict(data):
    data["outer"]["inner"] = None
    expected = {"outer": {}, "other": "can't touch this"}
    result = remove_none_values_from_dict(data)
    assert result == expected


def test_remove_duplicates_from_list_of_dicts():
    array: list[dict[str, Any]] = [
        {"name": "test", "region": None},
        {"name": "x"},
        {"name": "test", "region": None},
    ]
    cleaned = remove_duplicates_from_dict_list(array)
    assert cleaned == [{"name": "test", "region": None}, {"name": "x"}]


def test_remove_duplicates_from_list():
    array = ["foo", "bar", "bar", "bar", "spam"]
    cleaned = remove_duplicates_from_list(array)
    assert cleaned == ["foo", "bar", "spam"]
