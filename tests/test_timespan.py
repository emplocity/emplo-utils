from datetime import datetime

from dateutil.relativedelta import relativedelta

from emplo.timespan.period import CorrespondingPeriods
from emplo.timespan.serializer import (
    DatetimeTimespanRangeSerializer,
    StringTimespanRangeSerializer,
    TimestampTimespanRangeSerializer,
)


def test_corresponding_periods_for_today_period_current_timespan():
    corresponding_periods = CorrespondingPeriods(period_name="today")
    current_range = corresponding_periods.current_range
    start = datetime(
        corresponding_periods._now.year,
        corresponding_periods._now.month,
        corresponding_periods._now.day,
        0,
        0,
        0,
        0,
    )
    assert current_range.start == start
    assert current_range.end == corresponding_periods._now


def test_corresponding_periods_for_today_previous_timespan():
    corresponding_periods = CorrespondingPeriods(period_name="today")
    current_range = corresponding_periods.previous_range
    start = datetime(
        corresponding_periods._now.year,
        corresponding_periods._now.month,
        corresponding_periods._now.day,
        0,
        0,
        0,
        0,
    ) + relativedelta(days=-1)
    assert current_range.start == start
    assert current_range.end == corresponding_periods._now + relativedelta(days=-1)


def test_corresponding_periods_for_yesterday_period_current_timespan():
    corresponding_periods = CorrespondingPeriods(period_name="yesterday")
    current_range = corresponding_periods.current_range
    start = datetime(
        corresponding_periods._now.year,
        corresponding_periods._now.month,
        corresponding_periods._now.day,
        0,
        0,
        0,
        0,
    ) + relativedelta(days=-1)
    assert current_range.start == start
    assert current_range.end == start + relativedelta(days=+1)


def test_corresponding_periods_for_yesterday_period_previous_timespan():
    corresponding_periods = CorrespondingPeriods(period_name="yesterday")
    current_range = corresponding_periods.previous_range
    start = datetime(
        corresponding_periods._now.year,
        corresponding_periods._now.month,
        corresponding_periods._now.day,
        0,
        0,
        0,
        0,
    ) + relativedelta(days=-2)
    assert current_range.start == start
    assert current_range.end == start + relativedelta(days=+1)


def test_corresponding_periods_for_current_week_period_current_timespan():
    corresponding_periods = CorrespondingPeriods(period_name="current_week")
    current_range = corresponding_periods.current_range
    start = datetime(
        corresponding_periods._now.year,
        corresponding_periods._now.month,
        corresponding_periods._now.day,
        0,
        0,
        0,
        0,
    ) + relativedelta(days=-corresponding_periods._now.weekday())
    assert current_range.start == start
    assert current_range.end == corresponding_periods._now


def test_corresponding_periods_for_current_week_period_previous_timespan():
    corresponding_periods = CorrespondingPeriods(period_name="current_week")
    current_range = corresponding_periods.previous_range
    start = datetime(
        corresponding_periods._now.year,
        corresponding_periods._now.month,
        corresponding_periods._now.day,
        0,
        0,
        0,
        0,
    ) + relativedelta(days=-corresponding_periods._now.weekday() - 7)
    assert current_range.start == start
    assert current_range.end == corresponding_periods._now + relativedelta(days=-7)


def test_corresponding_periods_for_previous_week_period_current_timespan():
    corresponding_periods = CorrespondingPeriods(period_name="previous_week")
    current_range = corresponding_periods.current_range
    start = datetime(
        corresponding_periods._now.year,
        corresponding_periods._now.month,
        corresponding_periods._now.day,
        0,
        0,
        0,
        0,
    ) + relativedelta(days=-corresponding_periods._now.weekday() - 7)
    assert current_range.start == start
    assert current_range.end == start + relativedelta(days=7)


def test_corresponding_periods_for_previous_week_period_previous_timespan():
    corresponding_periods = CorrespondingPeriods(period_name="previous_week")
    current_range = corresponding_periods.previous_range
    start = datetime(
        corresponding_periods._now.year,
        corresponding_periods._now.month,
        corresponding_periods._now.day,
        0,
        0,
        0,
        0,
    ) + relativedelta(days=-corresponding_periods._now.weekday() - 14)
    assert current_range.start == start
    assert current_range.end == start + relativedelta(days=7)


def test_corresponding_periods_for_current_month_period_current_timespan():
    corresponding_periods = CorrespondingPeriods(period_name="current_month")
    current_range = corresponding_periods.current_range
    start = datetime(
        corresponding_periods._now.year, corresponding_periods._now.month, 1, 0, 0, 0, 0
    )
    assert current_range.start == start
    assert current_range.end == corresponding_periods._now


def test_corresponding_periods_for_current_month_period_previous_timespan():
    corresponding_periods = CorrespondingPeriods(period_name="current_month")
    current_range = corresponding_periods.previous_range
    start = datetime(
        corresponding_periods._now.year, corresponding_periods._now.month, 1, 0, 0, 0, 0
    ) + relativedelta(months=-1)
    assert current_range.start == start
    assert current_range.end == corresponding_periods._now + relativedelta(months=-1)


def test_corresponding_periods_for_previous_month_period_current_timespan():
    corresponding_periods = CorrespondingPeriods(period_name="previous_month")
    current_range = corresponding_periods.current_range
    start = datetime(
        corresponding_periods._now.year, corresponding_periods._now.month, 1, 0, 0, 0, 0
    ) + relativedelta(months=-1)
    assert current_range.start == start
    assert current_range.end == start + relativedelta(months=1)


def test_corresponding_periods_for_previous_month_period_previous_timespan():
    corresponding_periods = CorrespondingPeriods(period_name="previous_month")
    current_range = corresponding_periods.previous_range
    start = datetime(
        corresponding_periods._now.year, corresponding_periods._now.month, 1, 0, 0, 0, 0
    ) + relativedelta(months=-2)
    assert current_range.start == start
    assert current_range.end == start + relativedelta(months=1)


def test_corresponding_periods_for_entire_period():
    corresponding_periods = CorrespondingPeriods(period_name="entire")
    current_range = corresponding_periods.current_range
    previous_range = corresponding_periods.previous_range
    assert current_range.start == previous_range.start
    assert current_range.end == previous_range.end


def test_stats_serialize_range_isofromat():
    corresponding_periods = CorrespondingPeriods(period_name="entire")
    serialized_range = StringTimespanRangeSerializer(
        corresponding_periods.current_range
    ).serialize()
    assert "start" in serialized_range
    assert "end" in serialized_range
    assert isinstance(serialized_range["start"], str) is True
    assert isinstance(serialized_range["end"], str) is True


def test_stats_serialize_range_timestamp():
    corresponding_periods = CorrespondingPeriods(period_name="entire")
    serialized_range = TimestampTimespanRangeSerializer(
        corresponding_periods.current_range
    ).serialize()
    assert "start" in serialized_range
    assert "end" in serialized_range
    assert isinstance(serialized_range["start"], int) is True
    assert isinstance(serialized_range["end"], int) is True


def test_stats_serialize_range_datetime():
    corresponding_periods = CorrespondingPeriods(period_name="entire")
    serialized_range = DatetimeTimespanRangeSerializer(
        corresponding_periods.current_range
    ).serialize()
    assert "start" in serialized_range
    assert "end" in serialized_range
    assert isinstance(serialized_range["start"], datetime) is True
    assert isinstance(serialized_range["end"], datetime) is True
