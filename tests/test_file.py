import io
from unittest.mock import patch

import pytest
from werkzeug.datastructures import FileStorage

from emplo.file import FileIsTooLarge, FileSizeValidator, FileTypeError, FileUploader


def test_max_size_file_exceeded_by_file():
    size_limit = 10  # kB
    file_storage = FileStorage(
        stream=io.BytesIO(b"*" * 1024 * size_limit), name="test.txt"
    )
    validator = FileSizeValidator(max_size_kb=size_limit - 1)
    assert validator.validate(file_storage) is False


def test_max_size_file_not_exceeded_by_file():
    size_limit = 10  # kB
    file_storage = FileStorage(
        stream=io.BytesIO(b"*" * 1024 * size_limit), name="test.txt"
    )
    validator = FileSizeValidator(max_size_kb=size_limit + 1)
    assert validator.validate(file_storage) is True


def test_file_uploader_raise_when_input_file_is_bad_type():
    file_uploader = FileUploader(max_size_kb=1000)

    with pytest.raises(FileTypeError):
        file_uploader.upload(b"xyass12")  # type: ignore


@patch("emplo.file.FileSizeValidator.validate")
def test_file_uploader_raises_file_is_too_large(validate):
    validate.return_value = False
    file_uploader = FileUploader(max_size_kb=1000)

    with pytest.raises(FileIsTooLarge):
        file_uploader.upload("http://myurl.photo")
